package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/bhvinh3004/publishing-package/service"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"
)

func main() {
	router := gin.Default()
	dsn := "host=localhost user=postgres password=secret dbname=postgres port=5432 sslmode=disable TimeZone=Asia/Shanghai"
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal("can't connect to database")
	}
	v1 := router.Group("api")
	service.CreatePublishingMetaAPI(db, v1)
	router.Run(":8080")
}

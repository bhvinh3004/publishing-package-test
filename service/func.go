package service

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/bhvinh3004/publishing-package/handlers"
	"gitlab.com/bhvinh3004/publishing-package/repo"
	"gitlab.com/jf72vc/jfpackage/jfcommon.git/ginext"
	"gorm.io/gorm"
)

func CreatePublishingMetaAPI(db *gorm.DB, apiGroup *gin.RouterGroup) {
	//Publishing
	repoPublishing := repo.NewPublishingRepo(db)
	handlePublishing := handlers.NewPublishingHandlers(repoPublishing)
	//repeat
	repoRepeat := repo.NewRepeatRepo(db)
	handleRepeat := handlers.NewRepeatHandlers(repoRepeat)
	//repeat_meta
	repoRepeatMeta := repo.NewRepeatMetaRepo(db)
	handleRepeatMeta := handlers.NewRepeatMetaHandlers(repoRepeatMeta)
	//schedule_time
	repoScheduleTime := repo.NewScheduleTimeRepo(db)
	handleScheduleTime := handlers.NewScheduleTimeHandlers(repoScheduleTime)

	v1 := apiGroup.Group("v1")
	{
		//Publishing
		v1.POST("/publishing/create", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handlePublishing.Create))
		v1.GET("/publishing/get-one/:id", ginext.WrapHandler(handlePublishing.GetOneById))
		v1.PUT("/publishing/update/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handlePublishing.Update))
		v1.DELETE("/publishing/delete/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handlePublishing.Delete))
		v1.GET("/publishing/get-list", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handlePublishing.GetList))
		//	//repeat
		v1.POST("/repeat/create", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRepeat.Create))
		v1.GET("/repeat/get-one/:id", ginext.WrapHandler(handleRepeat.GetOneById))
		v1.PUT("/repeat/update/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRepeat.Update))
		v1.DELETE("/repeat/delete/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRepeat.Delete))
		v1.GET("/repeat/get-list", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRepeat.GetList))
		//	//repeat_meta
		v1.POST("/repeat_meta/create", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRepeatMeta.Create))
		v1.GET("/repeat_meta/get-one/:id", ginext.WrapHandler(handleRepeatMeta.GetOneById))
		v1.PUT("/repeat_meta/update/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRepeatMeta.Update))
		v1.DELETE("/repeat_meta/delete/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRepeatMeta.Delete))
		v1.GET("/repeat_meta/get-list", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRepeatMeta.GetList))
		//	//schedule_time
		v1.POST("/schedule_time/create", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleScheduleTime.Create))
		v1.GET("/schedule_time/get-one/:id", ginext.WrapHandler(handleScheduleTime.GetOneById))
		v1.PUT("/schedule_time/update/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleScheduleTime.Update))
		v1.DELETE("/schedule_time/delete/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleScheduleTime.Delete))
		v1.GET("/schedule_time/get-list", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleScheduleTime.GetList))
	}
}

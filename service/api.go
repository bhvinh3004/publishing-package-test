package service

import (
	model "gitlab.com/bhvinh3004/publishing-package/models"
	"gorm.io/gorm"
)

func MigratePublishingMetaModel(tx *gorm.DB) error {
	models := []interface{}{
		model.Publishing{},
		model.Repeat{},
		model.RepeatMeta{},
		model.ScheduleTime{},
	}
	for _, s := range models {
		err := tx.AutoMigrate(s)
		if err != nil {
			return err
		}
	}
	return nil
}

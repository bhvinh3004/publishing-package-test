package handlers

import (
	"github.com/google/uuid"
	model "gitlab.com/bhvinh3004/publishing-package/models"
	"gitlab.com/bhvinh3004/publishing-package/repo"
	"gitlab.com/bhvinh3004/publishing-package/utils"
	"gitlab.com/jf72vc/jfpackage/jfcommon.git/ginext"
	"gitlab.com/jf72vc/jfpackage/jfcommon.git/logger"
	"net/http"
)

type RepeatMetaHandlers struct {
	obRepo *repo.RepeatMeta
}

func NewRepeatMetaHandlers(repo *repo.RepeatMeta) *RepeatMetaHandlers {
	return &RepeatMetaHandlers{obRepo: repo}
}

func (h *RepeatMetaHandlers) Create(r *ginext.Request) (*ginext.Response, error) {
	strRepeatMetaID := ginext.GetUserID(r.GinCtx)
	owner, err := uuid.Parse(strRepeatMetaID)
	if err != nil {
		return nil, ginext.NewError(http.StatusUnauthorized, err.Error())
	}
	req := model.RepeatMetaRequest{}
	r.MustBind(&req)
	ob := &model.RepeatMeta{
		BaseModel: model.BaseModel{
			CreatorID: &owner,
		},
	}
	if err = h.obRepo.Create(r.Context(), ob); err != nil {
		return nil, ginext.NewError(http.StatusInternalServerError, err.Error())
	}
	return ginext.NewResponseData(http.StatusOK, ob), nil
}

func (h *RepeatMetaHandlers) GetOneById(r *ginext.Request) (*ginext.Response, error) {
	ID := &uuid.UUID{}
	if ID := utils.ParseIDFromUri(r.GinCtx); ID == nil {
		return nil, ginext.NewError(http.StatusBadRequest, "Wrong ID")
	}
	ob, err := h.obRepo.GetByID(r.Context(), *ID)
	if err != nil {
		return nil, ginext.NewError(http.StatusNotFound, err.Error())
	}
	r.MustNoError(err)
	return ginext.NewResponseData(http.StatusOK, ob), nil
}

func (h *RepeatMetaHandlers) GetList(r *ginext.Request) (*ginext.Response, error) {
	req := model.RepeatMetaListRequest{}
	r.MustBind(&req)

	filter := &model.RepeatMetaFilter{
		RepeatMetaListRequest: req,
		Pager:                 ginext.NewPagerWithGinCtx(r.GinCtx),
	}
	result, err := h.obRepo.List(r.Context(), filter)
	if err != nil {
		return nil, ginext.NewError(http.StatusInternalServerError, err.Error())
	}
	r.MustNoError(err)
	return ginext.NewResponseWithPager(http.StatusOK, result.Records, result.Filter.Pager), nil
}

func (h *RepeatMetaHandlers) Update(r *ginext.Request) (*ginext.Response, error) {
	l := logger.WithCtx(r.Context(), "RepeatMeta.Update")
	req := model.RepeatMetaRequest{}
	strUserID := ginext.GetUserID(r.GinCtx)
	currentUser, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(http.StatusUnauthorized, err.Error())
	}
	if req.ID = utils.ParseIDFromUri(r.GinCtx); req.ID == nil {
		return nil, ginext.NewError(http.StatusBadRequest, "Wrong ID")
	}
	r.MustBind(&req)

	ob, err := h.obRepo.GetByID(r.Context(), *req.ID)
	if err != nil {
		l.WithError(err).WithField("repeatMeta.id", req.ID).Error("failed to query item1")
		return nil, ginext.NewError(http.StatusNotFound, err.Error())
	}
	ob.UpdaterID = &currentUser

	if err = h.obRepo.UpdateByID(r.Context(), ob); err != nil {
		return nil, ginext.NewError(http.StatusInternalServerError, err.Error())
	}
	return ginext.NewResponseData(http.StatusOK, ob), nil
}

func (h *RepeatMetaHandlers) Delete(r *ginext.Request) (*ginext.Response, error) {
	strUserID := ginext.GetUserID(r.GinCtx)
	currentUser, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(http.StatusUnauthorized, err.Error())
	}

	req := model.RepeatMetaRequest{}
	if req.ID = utils.ParseIDFromUri(r.GinCtx); req.ID == nil {
		return nil, ginext.NewError(http.StatusBadRequest, "Wrong ID")
	}
	ob, err := h.obRepo.GetByID(r.Context(), *req.ID)
	if err != nil {
		return nil, ginext.NewError(http.StatusBadRequest, err.Error())
	}
	ob.UpdaterID = &currentUser
	r.MustNoError(h.obRepo.Delete(r.Context(), ob))
	return ginext.NewResponse(http.StatusOK), nil
}

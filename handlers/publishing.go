package handlers

import (
	"github.com/google/uuid"
	model "gitlab.com/bhvinh3004/publishing-package/models"
	"gitlab.com/bhvinh3004/publishing-package/repo"
	"gitlab.com/bhvinh3004/publishing-package/utils"
	"gitlab.com/jf72vc/jfpackage/jfcommon.git/ginext"
	"gitlab.com/jf72vc/jfpackage/jfcommon.git/logger"
	"net/http"
)

//publishing
//- /api/v1/{tên bảng}/create
//- /api/v1/{tên bảng}/get-one/:id
//- /api/v1/{tên bảng}/get-list
//- /api/v1/{tên bảng}/update/:id
//- /api/v1/{tên bảng}/delete

type PublishingHandlers struct {
	obRepo *repo.Publishing
}

func NewPublishingHandlers(repo *repo.Publishing) *PublishingHandlers {
	return &PublishingHandlers{obRepo: repo}
}

func (h *PublishingHandlers) Create(r *ginext.Request) (*ginext.Response, error) {
	strPublishingID := ginext.GetUserID(r.GinCtx)
	owner, err := uuid.Parse(strPublishingID)
	if err != nil {
		return nil, ginext.NewError(http.StatusUnauthorized, err.Error())
	}
	req := model.PublishingRequest{}
	r.MustBind(&req)
	ob := &model.Publishing{
		BaseModel: model.BaseModel{
			CreatorID: &owner,
		},
	}
	if err = h.obRepo.Create(r.Context(), ob); err != nil {
		return nil, ginext.NewError(http.StatusInternalServerError, err.Error())
	}
	return ginext.NewResponseData(http.StatusOK, ob), nil
}

func (h *PublishingHandlers) GetOneById(r *ginext.Request) (*ginext.Response, error) {
	ID := &uuid.UUID{}
	if ID := utils.ParseIDFromUri(r.GinCtx); ID == nil {
		return nil, ginext.NewError(http.StatusBadRequest, "Wrong ID")
	}
	ob, err := h.obRepo.GetByID(r.Context(), *ID)
	if err != nil {
		return nil, ginext.NewError(http.StatusNotFound, err.Error())
	}
	r.MustNoError(err)
	return ginext.NewResponseData(http.StatusOK, ob), nil
}

func (h *PublishingHandlers) GetList(r *ginext.Request) (*ginext.Response, error) {
	req := model.PublishingListRequest{}
	r.MustBind(&req)

	filter := &model.PublishingFilter{
		PublishingListRequest: req,
		Pager:                 ginext.NewPagerWithGinCtx(r.GinCtx),
	}
	result, err := h.obRepo.List(r.Context(), filter)
	if err != nil {
		return nil, ginext.NewError(http.StatusInternalServerError, err.Error())
	}
	r.MustNoError(err)
	return ginext.NewResponseWithPager(http.StatusOK, result.Records, result.Filter.Pager), nil
}

func (h *PublishingHandlers) Update(r *ginext.Request) (*ginext.Response, error) {
	l := logger.WithCtx(r.Context(), "Publishing.Update")
	req := model.PublishingRequest{}
	strUserID := ginext.GetUserID(r.GinCtx)
	currentUser, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(http.StatusUnauthorized, err.Error())
	}
	if req.ID = utils.ParseIDFromUri(r.GinCtx); req.ID == nil {
		return nil, ginext.NewError(http.StatusBadRequest, "Wrong ID")
	}
	r.MustBind(&req)

	ob, err := h.obRepo.GetByID(r.Context(), *req.ID)
	if err != nil {
		l.WithError(err).WithField("publishing.id", req.ID).Error("failed to query item1")
		return nil, ginext.NewError(http.StatusNotFound, err.Error())
	}
	ob.UpdaterID = &currentUser

	if err = h.obRepo.UpdateByID(r.Context(), ob); err != nil {
		return nil, ginext.NewError(http.StatusInternalServerError, err.Error())
	}
	return ginext.NewResponseData(http.StatusOK, ob), nil
}

func (h *PublishingHandlers) Delete(r *ginext.Request) (*ginext.Response, error) {
	strUserID := ginext.GetUserID(r.GinCtx)
	currentUser, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(http.StatusUnauthorized, err.Error())
	}

	req := model.PublishingRequest{}
	if req.ID = utils.ParseIDFromUri(r.GinCtx); req.ID == nil {
		return nil, ginext.NewError(http.StatusBadRequest, "Wrong ID")
	}
	ob, err := h.obRepo.GetByID(r.Context(), *req.ID)
	if err != nil {
		return nil, ginext.NewError(http.StatusBadRequest, err.Error())
	}
	ob.UpdaterID = &currentUser
	r.MustNoError(h.obRepo.Delete(r.Context(), ob))
	return ginext.NewResponse(http.StatusOK), nil
}

package handlers

import (
	"github.com/google/uuid"
	model "gitlab.com/bhvinh3004/publishing-package/models"
	"gitlab.com/bhvinh3004/publishing-package/repo"
	"gitlab.com/bhvinh3004/publishing-package/utils"
	"gitlab.com/jf72vc/jfpackage/jfcommon.git/ginext"
	"gitlab.com/jf72vc/jfpackage/jfcommon.git/logger"
	"net/http"
)

type ScheduleTimeHandlers struct {
	obRepo *repo.ScheduleTime
}

func NewScheduleTimeHandlers(repo *repo.ScheduleTime) *ScheduleTimeHandlers {
	return &ScheduleTimeHandlers{obRepo: repo}
}

func (h *ScheduleTimeHandlers) Create(r *ginext.Request) (*ginext.Response, error) {
	strScheduleTimeID := ginext.GetUserID(r.GinCtx)
	owner, err := uuid.Parse(strScheduleTimeID)
	if err != nil {
		return nil, ginext.NewError(http.StatusUnauthorized, err.Error())
	}
	req := model.ScheduleTimeRequest{}
	r.MustBind(&req)
	ob := &model.ScheduleTime{
		BaseModel: model.BaseModel{
			CreatorID: &owner,
		},
	}
	if err = h.obRepo.Create(r.Context(), ob); err != nil {
		return nil, ginext.NewError(http.StatusInternalServerError, err.Error())
	}
	return ginext.NewResponseData(http.StatusOK, ob), nil
}

func (h *ScheduleTimeHandlers) GetOneById(r *ginext.Request) (*ginext.Response, error) {
	ID := &uuid.UUID{}
	if ID := utils.ParseIDFromUri(r.GinCtx); ID == nil {
		return nil, ginext.NewError(http.StatusBadRequest, "Wrong ID")
	}
	ob, err := h.obRepo.GetByID(r.Context(), *ID)
	if err != nil {
		return nil, ginext.NewError(http.StatusNotFound, err.Error())
	}
	r.MustNoError(err)
	return ginext.NewResponseData(http.StatusOK, ob), nil
}

func (h *ScheduleTimeHandlers) GetList(r *ginext.Request) (*ginext.Response, error) {
	req := model.ScheduleTimeListRequest{}
	r.MustBind(&req)

	filter := &model.ScheduleTimeFilter{
		ScheduleTimeListRequest: req,
		Pager:                   ginext.NewPagerWithGinCtx(r.GinCtx),
	}
	result, err := h.obRepo.List(r.Context(), filter)
	if err != nil {
		return nil, ginext.NewError(http.StatusInternalServerError, err.Error())
	}
	r.MustNoError(err)
	return ginext.NewResponseWithPager(http.StatusOK, result.Records, result.Filter.Pager), nil
}

func (h *ScheduleTimeHandlers) Update(r *ginext.Request) (*ginext.Response, error) {
	l := logger.WithCtx(r.Context(), "scheduletime.Update")
	req := model.ScheduleTimeRequest{}
	strUserID := ginext.GetUserID(r.GinCtx)
	currentUser, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(http.StatusUnauthorized, err.Error())
	}
	if req.ID = utils.ParseIDFromUri(r.GinCtx); req.ID == nil {
		return nil, ginext.NewError(http.StatusBadRequest, "Wrong ID")
	}
	r.MustBind(&req)

	ob, err := h.obRepo.GetByID(r.Context(), *req.ID)
	if err != nil {
		l.WithError(err).WithField("scheduletime.id", req.ID).Error("failed to query item1")
		return nil, ginext.NewError(http.StatusNotFound, err.Error())
	}
	ob.UpdaterID = &currentUser

	if err = h.obRepo.UpdateByID(r.Context(), ob); err != nil {
		return nil, ginext.NewError(http.StatusInternalServerError, err.Error())
	}
	return ginext.NewResponseData(http.StatusOK, ob), nil
}

func (h *ScheduleTimeHandlers) Delete(r *ginext.Request) (*ginext.Response, error) {
	strUserID := ginext.GetUserID(r.GinCtx)
	currentUser, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(http.StatusUnauthorized, err.Error())
	}

	req := model.ScheduleTimeRequest{}
	if req.ID = utils.ParseIDFromUri(r.GinCtx); req.ID == nil {
		return nil, ginext.NewError(http.StatusBadRequest, "Wrong ID")
	}
	ob, err := h.obRepo.GetByID(r.Context(), *req.ID)
	if err != nil {
		return nil, ginext.NewError(http.StatusBadRequest, err.Error())
	}
	ob.UpdaterID = &currentUser
	r.MustNoError(h.obRepo.Delete(r.Context(), ob))
	return ginext.NewResponse(http.StatusOK), nil
}

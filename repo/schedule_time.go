package repo

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	model "gitlab.com/bhvinh3004/publishing-package/models"
	"gitlab.com/bhvinh3004/publishing-package/utils"
	"gitlab.com/jf72vc/jfpackage/jfcommon.git/logger"
	"gorm.io/gorm"
)

// ScheduleTime
// - /api/v1/{tên bảng}/create
// - /api/v1/{tên bảng}/get-one/:id
// - /api/v1/{tên bảng}/get-list
// - /api/v1/{tên bảng}/update/:id
// - /api/v1/{tên bảng}/delete

type ScheduleTime struct {
	db *gorm.DB
}

func NewScheduleTimeRepo(db *gorm.DB) *ScheduleTime {
	return &ScheduleTime{db: db}
}

func (r *ScheduleTime) DBWithTimeout(ctx context.Context) (*gorm.DB, context.CancelFunc) {
	ctx, cancel := context.WithTimeout(ctx, generalQueryTimeout)
	return r.db.WithContext(ctx), cancel
}

func (r *ScheduleTime) Create(param interface{}, data *model.ScheduleTime) error {
	var (
		tx     *gorm.DB
		cancel context.CancelFunc
	)
	switch v := param.(type) {
	case context.Context:
		tx, cancel = r.DBWithTimeout(v)
		defer cancel()
	case *gorm.DB:
		tx = v
	default:
		return fmt.Errorf("invalid parameter")
	}
	return tx.Create(data).Error

}

func (r *ScheduleTime) List(param interface{}, f *model.ScheduleTimeFilter) (*model.ScheduleTimeFilterResult, error) {
	var (
		tx     *gorm.DB
		cancel context.CancelFunc
	)
	switch v := param.(type) {
	case context.Context:
		tx, cancel = r.DBWithTimeout(v)
		defer cancel()
	case *gorm.DB:
		tx = v
	default:
		return nil, fmt.Errorf("invalid parameter")
	}
	log := logger.WithCtx(tx.Statement.Context, "RepeatMeta.Filter")
	tx = tx.Model(&model.ScheduleTimeFilter{})

	op := tx.Where
	tx = utils.FilterIfNotNil(f.CreatorID, tx, op, "creator_id = ?")
	tx = utils.FilterIfNotNil(f.Priority, tx, op, "priority = ?")
	tx = utils.FilterIfNotNil(f.PublishingID, tx, op, "publishing_id = ?")
	tx = utils.FilterIfNotNil(f.StartTime, tx, op, "start_time = ?")
	tx = utils.FilterIfNotNil(f.EndTime, tx, op, "end_time = ?")
	result := &model.ScheduleTimeFilterResult{
		Filter:  f,
		Records: []*model.ScheduleTime{},
	}
	pager := result.Filter.Pager

	tx = pager.DoQuery(&result.Records, tx)
	if tx.Error != nil {
		log.WithError(tx.Error).Error("error while filter object meta")
	}
	return result, tx.Error
}

func (r *ScheduleTime) UpdateByID(param interface{}, ob *model.ScheduleTime) error {
	var (
		tx     *gorm.DB
		cancel context.CancelFunc
	)
	switch v := param.(type) {
	case context.Context:
		tx, cancel = r.DBWithTimeout(v)
		defer cancel()
	case *gorm.DB:
		tx = v
	default:
		return fmt.Errorf("invalid parameter")
	}
	return tx.Where("id = ?", ob.ID).Save(&ob).Error
}

func (r *ScheduleTime) GetByID(param interface{}, id uuid.UUID) (*model.ScheduleTime, error) {
	var (
		tx     *gorm.DB
		cancel context.CancelFunc
	)
	switch v := param.(type) {
	case context.Context:
		tx, cancel = r.DBWithTimeout(v)
		defer cancel()
	case *gorm.DB:
		tx = v
	default:
		return nil, fmt.Errorf("invalid parameter")
	}

	o := &model.ScheduleTime{}

	err := tx.First(&o, id).Error
	return o, err
}

func (r *ScheduleTime) Delete(param interface{}, d *model.ScheduleTime) error {
	var (
		tx     *gorm.DB
		cancel context.CancelFunc
	)
	switch v := param.(type) {
	case context.Context:
		tx, cancel = r.DBWithTimeout(v)
		defer cancel()
	case *gorm.DB:
		tx = v
	default:
		return fmt.Errorf("invalid parameter")
	}
	return tx.Delete(&d).Error
}

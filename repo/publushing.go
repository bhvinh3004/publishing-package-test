package repo

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	model "gitlab.com/bhvinh3004/publishing-package/models"
	"gitlab.com/bhvinh3004/publishing-package/utils"
	"gitlab.com/jf72vc/jfpackage/jfcommon.git/logger"
	"gorm.io/gorm"
)

//publishing
//- /api/v1/{tên bảng}/create - Done
//- /api/v1/{tên bảng}/get-one/:id -
//- /api/v1/{tên bảng}/get-list - Done
//- /api/v1/{tên bảng}/update/:id
//- /api/v1/{tên bảng}/delete

type Publishing struct {
	db *gorm.DB
}

func NewPublishingRepo(db *gorm.DB) *Publishing {
	return &Publishing{db: db}
}

func (r *Publishing) DBWithTimeout(ctx context.Context) (*gorm.DB, context.CancelFunc) {
	ctx, cancel := context.WithTimeout(ctx, generalQueryTimeout)
	return r.db.WithContext(ctx), cancel
}

func (r *Publishing) Create(param interface{}, data *model.Publishing) error {
	var (
		tx     *gorm.DB
		cancel context.CancelFunc
	)
	switch v := param.(type) {
	case context.Context:
		tx, cancel = r.DBWithTimeout(v)
		defer cancel()
	case *gorm.DB:
		tx = v
	default:
		return fmt.Errorf("invalid parameter")
	}
	return tx.Create(data).Error

}

func (r *Publishing) List(param interface{}, f *model.PublishingFilter) (*model.PublishingFilterResult, error) {
	var (
		tx     *gorm.DB
		cancel context.CancelFunc
	)
	switch v := param.(type) {
	case context.Context:
		tx, cancel = r.DBWithTimeout(v)
		defer cancel()
	case *gorm.DB:
		tx = v
	default:
		return nil, fmt.Errorf("invalid parameter")
	}
	log := logger.WithCtx(tx.Statement.Context, "Publishing.Filter")
	tx = tx.Model(&model.PublishingFilter{})

	op := tx.Where
	tx = utils.FilterIfNotNil(f.CreatorID, tx, op, "creator_id = ?")
	tx = utils.FilterIfNotNil(f.TransactionID, tx, op, "transaction_id = ?")
	tx = utils.FilterIfNotNil(f.Title, tx, op, "title = ?")
	tx = utils.FilterIfNotNil(f.StartTime, tx, op, "start_time = ?")
	tx = utils.FilterIfNotNil(f.EndTime, tx, op, "end_time = ?")
	tx = utils.FilterIfNotNil(f.IsActive, tx, op, " is_active = ?")
	tx = utils.FilterIfNotNil(f.Description, tx, op, "description = ?")
	result := &model.PublishingFilterResult{
		Filter:  f,
		Records: []*model.Publishing{},
	}
	pager := result.Filter.Pager

	tx = pager.DoQuery(&result.Records, tx)
	if tx.Error != nil {
		log.WithError(tx.Error).Error("error while filter object meta")
	}
	return result, tx.Error
}

func (r *Publishing) UpdateByID(param interface{}, ob *model.Publishing) error {
	var (
		tx     *gorm.DB
		cancel context.CancelFunc
	)
	switch v := param.(type) {
	case context.Context:
		tx, cancel = r.DBWithTimeout(v)
		defer cancel()
	case *gorm.DB:
		tx = v
	default:
		return fmt.Errorf("invalid parameter")
	}
	return tx.Where("id = ?", ob.ID).Save(&ob).Error
}

func (r *Publishing) GetByID(param interface{}, id uuid.UUID) (*model.Publishing, error) {
	var (
		tx     *gorm.DB
		cancel context.CancelFunc
	)
	switch v := param.(type) {
	case context.Context:
		tx, cancel = r.DBWithTimeout(v)
		defer cancel()
	case *gorm.DB:
		tx = v
	default:
		return nil, fmt.Errorf("invalid parameter")
	}

	o := &model.Publishing{}

	err := tx.First(&o, id).Error
	return o, err
}

func (r *Publishing) Delete(param interface{}, d *model.Publishing) error {
	var (
		tx     *gorm.DB
		cancel context.CancelFunc
	)
	switch v := param.(type) {
	case context.Context:
		tx, cancel = r.DBWithTimeout(v)
		defer cancel()
	case *gorm.DB:
		tx = v
	default:
		return fmt.Errorf("invalid parameter")
	}
	return tx.Delete(&d).Error
}

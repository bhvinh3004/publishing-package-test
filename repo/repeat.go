package repo

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	model "gitlab.com/bhvinh3004/publishing-package/models"
	"gitlab.com/bhvinh3004/publishing-package/utils"
	"gitlab.com/jf72vc/jfpackage/jfcommon.git/logger"
	"gorm.io/gorm"
)

//Repeat
//- /api/v1/{tên bảng}/create
//- /api/v1/{tên bảng}/get-one/:id
//- /api/v1/{tên bảng}/get-list
//- /api/v1/{tên bảng}/update/:id
//- /api/v1/{tên bảng}/delete

type Repeat struct {
	db *gorm.DB
}

func NewRepeatRepo(db *gorm.DB) *Repeat {
	return &Repeat{db: db}
}

func (r *Repeat) DBWithTimeout(ctx context.Context) (*gorm.DB, context.CancelFunc) {
	ctx, cancel := context.WithTimeout(ctx, generalQueryTimeout)
	return r.db.WithContext(ctx), cancel
}

func (r *Repeat) Create(param interface{}, data *model.Repeat) error {
	var (
		tx     *gorm.DB
		cancel context.CancelFunc
	)
	switch v := param.(type) {
	case context.Context:
		tx, cancel = r.DBWithTimeout(v)
		defer cancel()
	case *gorm.DB:
		tx = v
	default:
		return fmt.Errorf("invalid parameter")
	}
	return tx.Create(data).Error

}

func (r *Repeat) List(param interface{}, f *model.RepeatFilter) (*model.RepeatFilterResult, error) {
	var (
		tx     *gorm.DB
		cancel context.CancelFunc
	)
	switch v := param.(type) {
	case context.Context:
		tx, cancel = r.DBWithTimeout(v)
		defer cancel()
	case *gorm.DB:
		tx = v
	default:
		return nil, fmt.Errorf("invalid parameter")
	}
	log := logger.WithCtx(tx.Statement.Context, "Repeat.Filter")
	tx = tx.Model(&model.RepeatFilter{})

	op := tx.Where
	tx = utils.FilterIfNotNil(f.CreatorID, tx, op, "creator_id = ?")
	tx = utils.FilterIfNotNil(f.ScheduleTimeID, tx, op, "schedule_time_id = ?")
	tx = utils.FilterIfNotNil(f.Title, tx, op, "title = ?")
	tx = utils.FilterIfNotNil(f.Type, tx, op, "type = ?")
	tx = utils.FilterIfNotNil(f.RepeatOn, tx, op, "repeat_on = ?")
	tx = utils.FilterIfNotNil(f.Times, tx, op, " times = ?")
	tx = utils.FilterIfNotNil(f.IsActive, tx, op, "is_active = ?")
	result := &model.RepeatFilterResult{
		Filter:  f,
		Records: []*model.Repeat{},
	}
	pager := result.Filter.Pager

	tx = pager.DoQuery(&result.Records, tx)
	if tx.Error != nil {
		log.WithError(tx.Error).Error("error while filter object meta")
	}
	return result, tx.Error
}

func (r *Repeat) UpdateByID(param interface{}, ob *model.Repeat) error {
	var (
		tx     *gorm.DB
		cancel context.CancelFunc
	)
	switch v := param.(type) {
	case context.Context:
		tx, cancel = r.DBWithTimeout(v)
		defer cancel()
	case *gorm.DB:
		tx = v
	default:
		return fmt.Errorf("invalid parameter")
	}
	return tx.Where("id = ?", ob.ID).Save(&ob).Error
}

func (r *Repeat) GetByID(param interface{}, id uuid.UUID) (*model.Repeat, error) {
	var (
		tx     *gorm.DB
		cancel context.CancelFunc
	)
	switch v := param.(type) {
	case context.Context:
		tx, cancel = r.DBWithTimeout(v)
		defer cancel()
	case *gorm.DB:
		tx = v
	default:
		return nil, fmt.Errorf("invalid parameter")
	}

	o := &model.Repeat{}

	err := tx.First(&o, id).Error
	return o, err
}

func (r *Repeat) Delete(param interface{}, d *model.Repeat) error {
	var (
		tx     *gorm.DB
		cancel context.CancelFunc
	)
	switch v := param.(type) {
	case context.Context:
		tx, cancel = r.DBWithTimeout(v)
		defer cancel()
	case *gorm.DB:
		tx = v
	default:
		return fmt.Errorf("invalid parameter")
	}
	return tx.Delete(&d).Error
}

package repo

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	model "gitlab.com/bhvinh3004/publishing-package/models"
	"gitlab.com/bhvinh3004/publishing-package/utils"
	"gitlab.com/jf72vc/jfpackage/jfcommon.git/logger"
	"gorm.io/gorm"
)

// RepeatMeta
// - /api/v1/{tên bảng}/create
// - /api/v1/{tên bảng}/get-one/:id
// - /api/v1/{tên bảng}/get-list
// - /api/v1/{tên bảng}/update/:id
// - /api/v1/{tên bảng}/delete

type RepeatMeta struct {
	db *gorm.DB
}

func NewRepeatMetaRepo(db *gorm.DB) *RepeatMeta {
	return &RepeatMeta{db: db}
}

func (r *RepeatMeta) DBWithTimeout(ctx context.Context) (*gorm.DB, context.CancelFunc) {
	ctx, cancel := context.WithTimeout(ctx, generalQueryTimeout)
	return r.db.WithContext(ctx), cancel
}

func (r *RepeatMeta) Create(param interface{}, data *model.RepeatMeta) error {
	var (
		tx     *gorm.DB
		cancel context.CancelFunc
	)
	switch v := param.(type) {
	case context.Context:
		tx, cancel = r.DBWithTimeout(v)
		defer cancel()
	case *gorm.DB:
		tx = v
	default:
		return fmt.Errorf("invalid parameter")
	}
	return tx.Create(data).Error

}

func (r *RepeatMeta) List(param interface{}, f *model.RepeatMetaFilter) (*model.RepeatMetaFilterResult, error) {
	var (
		tx     *gorm.DB
		cancel context.CancelFunc
	)
	switch v := param.(type) {
	case context.Context:
		tx, cancel = r.DBWithTimeout(v)
		defer cancel()
	case *gorm.DB:
		tx = v
	default:
		return nil, fmt.Errorf("invalid parameter")
	}
	log := logger.WithCtx(tx.Statement.Context, "RepeatMeta.Filter")
	tx = tx.Model(&model.RepeatMetaFilter{})

	op := tx.Where
	tx = utils.FilterIfNotNil(f.CreatorID, tx, op, "creator_id = ?")
	tx = utils.FilterIfNotNil(f.RepeatID, tx, op, "repeat_id = ?")
	tx = utils.FilterIfNotNil(f.SettingMetaKey, tx, op, "setting_meta_key = ?")
	tx = utils.FilterIfNotNil(f.MetaKey, tx, op, "meta_key = ?")
	tx = utils.FilterIfNotNil(f.MetaValue, tx, op, "meta_value = ?")
	tx = utils.FilterIfNotNil(f.Description, tx, op, " description = ?")
	tx = utils.FilterIfNotNil(f.HardDeleteAt, tx, op, "hard_delete_at = ?")
	result := &model.RepeatMetaFilterResult{
		Filter:  f,
		Records: []*model.RepeatMeta{},
	}
	pager := result.Filter.Pager

	tx = pager.DoQuery(&result.Records, tx)
	if tx.Error != nil {
		log.WithError(tx.Error).Error("error while filter object meta")
	}
	return result, tx.Error
}

func (r *RepeatMeta) UpdateByID(param interface{}, ob *model.RepeatMeta) error {
	var (
		tx     *gorm.DB
		cancel context.CancelFunc
	)
	switch v := param.(type) {
	case context.Context:
		tx, cancel = r.DBWithTimeout(v)
		defer cancel()
	case *gorm.DB:
		tx = v
	default:
		return fmt.Errorf("invalid parameter")
	}
	return tx.Where("id = ?", ob.ID).Save(&ob).Error
}

func (r *RepeatMeta) GetByID(param interface{}, id uuid.UUID) (*model.RepeatMeta, error) {
	var (
		tx     *gorm.DB
		cancel context.CancelFunc
	)
	switch v := param.(type) {
	case context.Context:
		tx, cancel = r.DBWithTimeout(v)
		defer cancel()
	case *gorm.DB:
		tx = v
	default:
		return nil, fmt.Errorf("invalid parameter")
	}

	o := &model.RepeatMeta{}

	err := tx.First(&o, id).Error
	return o, err
}

func (r *RepeatMeta) Delete(param interface{}, d *model.RepeatMeta) error {
	var (
		tx     *gorm.DB
		cancel context.CancelFunc
	)
	switch v := param.(type) {
	case context.Context:
		tx, cancel = r.DBWithTimeout(v)
		defer cancel()
	case *gorm.DB:
		tx = v
	default:
		return fmt.Errorf("invalid parameter")
	}
	return tx.Delete(&d).Error
}

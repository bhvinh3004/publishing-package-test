package model

import (
	"github.com/google/uuid"
	"gitlab.com/jf72vc/jfpackage/jfcommon.git/ginext"
	"time"
)

type ScheduleTime struct {
	BaseModel
	Priority     int64      `json:"priority" gorm:"column:priority;type:numeric;not null"`
	PublishingID uuid.UUID  `json:"publishing_id" gorm:"column:publishing_id;type:uuid;not null;foreignKey:publishing_id;reference:id"`
	StartTime    time.Time  `json:"start_time" gorm:"column:start_time;type:timestamptz;not null"`
	EndTime      time.Time  `json:"end_time" gorm:"column:end_tme;type:timestamptz;null"`
	Publishing   Publishing `gorm:"foreignKey:publishing_id"`
}

func (ScheduleTime) TableName() string {
	return "schedule_times"
}

type ScheduleTimeRequest struct {
	ID           *uuid.UUID `json:"id,omitempty"`
	Priority     *int64     `json:"priority"`
	PublishingID *uuid.UUID `json:"publishing_id"`
	StartTime    *time.Time `json:"start_time"`
	EndTime      *time.Time `json:"end_time"`
}

type ScheduleTimeListRequest struct {
	CreatorID    *string    `json:"creator_id"`
	Priority     *int64     `json:"priority"`
	PublishingID *uuid.UUID `json:"publishing_id"`
	StartTime    *time.Time `json:"start_time"`
	EndTime      *time.Time `json:"end_time"`
}

type ScheduleTimeFilter struct {
	ScheduleTimeListRequest
	Pager *ginext.Pager
}

type ScheduleTimeFilterResult struct {
	Filter  *ScheduleTimeFilter
	Records []*ScheduleTime
}

package model

import (
	"github.com/google/uuid"
	"gitlab.com/jf72vc/jfpackage/jfcommon.git/ginext"
)

type Repeat struct {
	BaseModel
	ScheduleTimeID uuid.UUID              `json:"schedule_time_id" gorm:"column:schedule_time_id;type:uuid;not null"`
	Title          string                 `json:"title" gorm:"column:title;type:text;null"`
	Type           string                 `json:"type" gorm:"column:type;type:varchar(255);not null"`
	RepeatOn       map[string]interface{} `json:"repeat_on" gorm:"column:repeat_on;type:jsonb;null"`
	Times          int                    `json:"times" gorm:"column:times;type:int;not null"`
	IsActive       bool                   `json:"is_active" gorm:"column:is_active;type:bool;not null"`
	ScheduleTime   ScheduleTime           `gorm:"foreignKey:schedule_time_id"`
}

func (Repeat) TableName() string {
	return "repeats"
}

type RepeatRequest struct {
	ID             *uuid.UUID              `json:"id,omitempty"`
	ScheduleTimeID *uuid.UUID              `json:"schedule_time_id"`
	Title          *string                 `json:"title"`
	Type           *string                 `json:"type"`
	RepeatOn       *map[string]interface{} `json:"repeat_on"`
	Times          *int                    `json:"times"`
	IsActive       *bool                   `json:"is_active"`
}

type RepeatListRequest struct {
	CreatorID      *string                 `json:"creator_id" form:"creator_id"`
	ScheduleTimeID *uuid.UUID              `json:"schedule_time_id"`
	Title          *string                 `json:"title"`
	Type           *string                 `json:"type"`
	RepeatOn       *map[string]interface{} `json:"repeat_on"`
	Times          *int                    `json:"times"`
	IsActive       *bool                   `json:"is_active"`
}

type RepeatFilter struct {
	RepeatListRequest
	Pager *ginext.Pager
}

type RepeatFilterResult struct {
	Filter  *RepeatFilter
	Records []*Repeat
}

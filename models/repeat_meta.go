package model

import (
	"github.com/google/uuid"
	"gitlab.com/jf72vc/jfpackage/jfcommon.git/ginext"
	"time"
)

type RepeatMeta struct {
	BaseModel
	RepeatID       uuid.UUID `json:"repeat_id" gorm:"column:repeat_id;type:uuid;not null"`
	SettingMetaKey string    `json:"setting_meta_key" gorm:"column:setting_meta_key;type:varchar(255)"`
	MetaKey        string    `json:"meta_key" gorm:"column:meta_key;type:varchar(255);not null"`
	MetaValue      string    `json:"meta_value" gorm:"column:meta_value;type:text"`
	Description    string    `json:"description" gorm:"column:description;type:text"`
	HardDeleteAt   time.Time `json:"hard_delete_at" gorm:"column:hard_delete_at;type:timestamptz;null"`
	Repeat         Repeat    `gorm:"foreignKey:repeat_id"`
}

func (RepeatMeta) TableName() string {
	return "repeat_meta"
}

type RepeatMetaRequest struct {
	ID             *uuid.UUID `json:"id,omitempty"`
	RepeatID       *uuid.UUID `json:"repeat_id"`
	SettingMetaKey *string    `json:"setting_meta_key"`
	MetaKey        *string    `json:"meta_key"`
	MetaValue      *string    `json:"meta_value"`
	Description    *string    `json:"description"`
	HardDeleteAt   *time.Time `json:"hard_delete_at"`
}

type RepeatMetaListRequest struct {
	CreatorID      *string    `json:"creator_id" form:"creator_id"`
	RepeatID       *uuid.UUID `json:"repeat_id"`
	SettingMetaKey *string    `json:"setting_meta_key"`
	MetaKey        *string    `json:"meta_key"`
	MetaValue      *string    `json:"meta_value"`
	Description    *string    `json:"description"`
	HardDeleteAt   *time.Time `json:"hard_delete_at"`
}

type RepeatMetaFilter struct {
	RepeatMetaListRequest
	Pager *ginext.Pager
}

type RepeatMetaFilterResult struct {
	Filter  *RepeatMetaFilter
	Records []*RepeatMeta
}

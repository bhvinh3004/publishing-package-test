package model

import (
	"github.com/google/uuid"
	"gitlab.com/jf72vc/jfpackage/jfcommon.git/ginext"
	"time"
)

type Publishing struct {
	BaseModel
	TransactionID uuid.UUID `json:"transaction_id" gorm:"column:transaction_id;type:uuid;not null"`
	Title         string    `json:"title" gorm:"column:title;type:text;null"`
	StartTime     time.Time `json:"start_time" gorm:"column:start_time;type:time;not null"`
	EndTime       time.Time `json:"end_time" gorm:"column:end_time;type:time;null"`
	IsActive      bool      `json:"is_active" gorm:"column:is_active;type:bool;not null"`
	Description   string    `json:"description" gorm:"column:description;type:text;null"`
}

func (Publishing) TableName() string {
	return "publishings"
}

type PublishingRequest struct {
	ID            *uuid.UUID `json:"id,omitempty"`
	TransactionID *uuid.UUID `json:"transaction_id" `
	Title         *string    `json:"title" `
	StartTime     *time.Time `json:"start_time" `
	EndTime       *time.Time `json:"end_time" `
	IsActive      *bool      `json:"is_active" `
	Description   *string    `json:"description" `
}

type PublishingListRequest struct {
	CreatorID     *string    `json:"creator_id" form:"creator_id"`
	TransactionID *uuid.UUID `json:"transaction_id" `
	Title         *string    `json:"title" `
	StartTime     *time.Time `json:"start_time" `
	EndTime       *time.Time `json:"end_time" `
	IsActive      *bool      `json:"is_active" `
	Description   *string    `json:"description" `
}

type PublishingFilter struct {
	PublishingListRequest
	Pager *ginext.Pager
}

type PublishingFilterResult struct {
	Filter  *PublishingFilter
	Records []*Publishing
}
